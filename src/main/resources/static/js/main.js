$(document).ready(function () {

    $("#brand-form").submit(function (event) {

        //stop submit the form, we will post it manually.
        event.preventDefault();

        fire_ajax_submit();

    });

});

function fire_ajax_submit() {
	var search = {}
    search["name"] = $("#name").val();
    search["rating"] = $("#rating").val();

    $("#btn-brand").prop("disabled", true);
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/api/shop/brand",
        data: JSON.stringify(search),
        dataType: 'text',
        cache: false,
        timeout: 600000,
        success: function (data) {
         console.log("added : ", data);
         alert("added");

        },
        error: function (e) {

            var json = "<h4>Ajax Response</h4><pre>"
                + e.responseText + "</pre>";
            $('#feedback').html(json);

            console.log("ERROR : ", e);
            $("#btn-search").prop("disabled", false);

        }
    });

}