package com.training.exception;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ValidationErrorDto {

	private List<FieldErrorDto> fieldErrors = new ArrayList<>();

	public ValidationErrorDto() {

	}

	public void addFieldError(String path, String message) {
		FieldErrorDto error = new FieldErrorDto(path, message);
		fieldErrors.add(error);
	}
}
