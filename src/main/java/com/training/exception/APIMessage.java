package com.training.exception;

public class APIMessage {
	
	public static final String CATEGORY_NOT_FOUND = "CATEGORY_NOT_FOUND";
	
	public static final String BRAND_NOT_FOUND = "NO_BRAND_FOUND";
	
	public static final String PRODUCT_NOT_FOUND = "NO_PRODUCT_FOUND";
	
	
}
