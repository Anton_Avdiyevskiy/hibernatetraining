package com.training.exception;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CustomErrorResponseBody {
	
	private  String errorMessage;

	public CustomErrorResponseBody(String errorMessage) {
		super();
		this.errorMessage = errorMessage;
	}
	
}
