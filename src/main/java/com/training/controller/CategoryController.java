package com.training.controller;


import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.exception.CustomErrorResponseBody;
import com.training.model.Category;
import com.training.service.CategoryService;
import com.training.utils.MessageUtils;


@RestController
@RequestMapping("/api/shop")
public class CategoryController {

	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private MessageUtils messageUtils;

	@PostMapping(value = "/category", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createCategory(@RequestBody @Valid Category category, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			String validationMessage = messageUtils.generateValidationMessage(bindingResult.getFieldErrors());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new CustomErrorResponseBody(validationMessage));
		}
		categoryService.saveCategory(category);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	@GetMapping(value = "/category", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getAllCategories(@PathVariable Long id) {
		
		List<Category>categories = categoryService.getAllCategories();
		return new ResponseEntity<List<Category>>(categories, HttpStatus.OK);
	}

	@GetMapping(value = "/category/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getCategory(@PathVariable Long id) {
		Category category = categoryService.getCategory(id);
		
		return new ResponseEntity<Category>(category, HttpStatus.OK);
	}

	@DeleteMapping(value = "/category/{id}")
	public ResponseEntity<?> deleteCategory(@PathVariable Long id) {
		categoryService.deleteCategory(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PutMapping(value = "/category/{id}")
	public ResponseEntity<?> updateCategory(@RequestBody @Valid Category category, @PathVariable Long id,
			BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			String validationMessage = messageUtils.generateValidationMessage(bindingResult.getFieldErrors());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new CustomErrorResponseBody(validationMessage));
		}
		categoryService.updateCategory(id, category);

		return new ResponseEntity<Category>(HttpStatus.OK);
	}
}
