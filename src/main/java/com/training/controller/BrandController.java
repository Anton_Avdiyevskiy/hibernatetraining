package com.training.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.exception.CustomErrorResponseBody;
import com.training.model.Brand;
import com.training.model.Category;
import com.training.service.BrandService;
import com.training.utils.MessageUtils;

@RestController
@RequestMapping("/api/shop")
public class BrandController {

	@Autowired
	private  BrandService brandService;

	@Autowired
	private  MessageUtils messageUtils;


	@PostMapping(value = "/brand", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createBrand(@Valid @RequestBody Brand brand, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			String validationMessage = messageUtils.generateValidationMessage(bindingResult.getFieldErrors());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomErrorResponseBody(validationMessage));
		}
		brandService.saveBrand(brand);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@GetMapping(value = "/brand/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getBrand(@PathVariable long id) {
		Brand brand = brandService.getBrandById(id);
		return new ResponseEntity<Brand>(brand, HttpStatus.OK);
	}

	@DeleteMapping(value = "/brand/{id}")
	public ResponseEntity<?> deleteBrand(@PathVariable long id) {
		brandService.deleteBrand(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PutMapping(value = "/brand/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> updateBrand(@Valid @RequestBody Brand brand, @PathVariable long id,
			BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			String validationMessage = messageUtils.generateValidationMessage(bindingResult.getFieldErrors());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomErrorResponseBody(validationMessage));
		}

		brandService.updateBrand(id, brand);

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PutMapping(value = "/brand/{brandId}/category/{categoryId}")
	public ResponseEntity<?> addCategoryToBrand(@PathVariable long brandId, @PathVariable long categoryId) {
		brandService.addCategoryToBrand(brandId, categoryId);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@GetMapping(value = "/brand/{brandId}/category", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getCategoriesOfBrand(@PathVariable long brandId) {
		List<Category> categories = brandService.getCategoriesOfBrand(brandId);
		return new ResponseEntity<List<Category>>(categories, HttpStatus.OK);
	}

	@GetMapping(value = "/fuck", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> fuck() {
		String o = "asd";
		return new ResponseEntity<String>(o, HttpStatus.OK);
	}
}
