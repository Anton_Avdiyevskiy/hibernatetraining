package com.training.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.exception.CustomErrorResponseBody;
import com.training.model.Product;
import com.training.service.ProductService;
import com.training.utils.MessageUtils;

@RestController
@RequestMapping(value = "/api/shop")
public class ProductController {

	private ProductService productService;
	
	private MessageUtils messageUtil;
	
	@Autowired
	public ProductController(ProductService productService,
			MessageUtils messageUtil) {
		this.productService = productService;
		this.messageUtil = messageUtil;
	}
	
	@GetMapping(value = "/product/{productId}")
	public ResponseEntity<?> getProduct(@PathVariable long productId){
		Product product = productService.getProduct(productId);
		return new ResponseEntity<Product>(product, HttpStatus.OK);
	}
	
	@DeleteMapping(value = "category/{categoryId}/product/{productId}")
	public ResponseEntity<?> deleteProduct(@PathVariable long productId,@PathVariable long categoryId){
		productService.deleteProduct(productId, categoryId);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@PostMapping(value = "category/{categoryId}/product")
	public ResponseEntity<?> createProduct(@PathVariable long categoryId,
			@RequestBody@Valid Product product,
			BindingResult bindingResult){
		if(bindingResult.hasErrors()) {
			String validationMessage = messageUtil.generateValidationMessage(bindingResult.getFieldErrors());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new CustomErrorResponseBody(validationMessage));
		}
		productService.save(product, categoryId);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	@PutMapping(value = "/product/{productId}")
	public ResponseEntity<?> updateProduct(@PathVariable long productId,
			@RequestBody@Valid Product product,
			BindingResult bindingResult){
		if(bindingResult.hasErrors()) {
			String validationMessage = messageUtil.generateValidationMessage(bindingResult.getFieldErrors());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new CustomErrorResponseBody(validationMessage));
		}
		productService.updateProduct(product, productId);;
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
}
