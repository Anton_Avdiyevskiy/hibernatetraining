//package com.training.controller;
//
//import javax.validation.Valid;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
//import org.springframework.validation.BindingResult;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.training.exception.CustomErrorResponseBody;
//import com.training.model.Account;
//import com.training.service.UserService;
//import com.training.utils.MessageUtils;
//
//@RestController
//@RequestMapping(value = "/api/shop")
//public class UserController {
//
//	private UserService userService;
//
//	private MessageUtils messageUtils;
//
//	@Autowired
//	public UserController(UserService userService, MessageUtils messageUtils) {
//		this.userService = userService;
//		this.messageUtils = messageUtils;
//	}
//
//	@PostMapping(value = "/registration" , consumes = MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity<?> createUser(@RequestBody @Valid Account account, BindingResult bindingResult) {
//		if (bindingResult.hasErrors()) {
//			String validationMessage = messageUtils.generateValidationMessage(bindingResult.getFieldErrors());
//			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomErrorResponseBody(validationMessage));
//		}
//		userService.saveUser(account);
//		return new ResponseEntity<Account>(account,HttpStatus.CREATED);
//	}
//
//}
