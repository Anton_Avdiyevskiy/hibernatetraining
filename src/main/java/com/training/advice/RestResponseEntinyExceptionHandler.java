package com.training.advice;



import org.springframework.dao.EmptyResultDataAccessException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.training.exception.CustomErrorResponseBody;
import com.training.exception.NotFoundEntityException;

@ControllerAdvice
public class RestResponseEntinyExceptionHandler extends ResponseEntityExceptionHandler {

	@ResponseBody
	@ExceptionHandler(value = { EmptyResultDataAccessException.class, NotFoundEntityException.class })
	public ResponseEntity<?> handleEmptyResultException(RuntimeException ex, WebRequest request) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomErrorResponseBody(ex.getMessage()));

	}
}
