package com.training.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Entity
public class Category {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@NotNull(message = "name of category is required")
	@Size(min = 1, max = 30)
	private String name;

	@NotNull(message = "")
	private int amountOfGoods;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "category_brand", joinColumns = { @JoinColumn(name = "category_id") }, inverseJoinColumns = {
			@JoinColumn(name = "brand_id") })
	@JsonIgnore
	private List<Brand> brands;
	
	@OneToMany(mappedBy = "category")
	private List<Product>products;

	public Category(String name) {
		super();
		this.name = name;
	}

}
