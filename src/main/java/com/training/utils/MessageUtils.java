package com.training.utils;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.validation.FieldError;

@Component
public class MessageUtils {

	public String generateValidationMessage(List<FieldError> fieldErrors) {
		StringBuilder messageBuilder = new StringBuilder();
		String validationError = fieldErrors.stream().map((fieldError) -> {
			String message = messageBuilder.append(fieldError.getField()).append(" ")
					.append(fieldError.getDefaultMessage()).toString();
			return message;
		}).findFirst().get();
		return validationError;
	}
}
