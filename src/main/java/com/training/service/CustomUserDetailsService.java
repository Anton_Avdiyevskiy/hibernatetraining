//package com.training.service;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.stereotype.Service;
//
//import com.training.model.Account;
//import com.training.model.UserPrincipal;
//
//@Service
//public class CustomUserDetailsService implements UserDetailsService {
//
//	private UserRepository userRepo;
//
//	@Autowired
//	public CustomUserDetailsService(UserRepository userRepo) {
//
//		this.userRepo = userRepo;
//	}
//
//	@Override
//	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//		Account account = userRepo.findByUsername(username);
//		if (account != null) {
//			return new UserPrincipal(account);
//		}
//		throw new UsernameNotFoundException("User '" + username + "' not found");
//	}
//
//}
