package com.training.service;



import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.training.model.Brand;

@Repository
public interface BrandRepository extends CrudRepository<Brand, Long> {
	
	//public List<Category> getCategories();

}
