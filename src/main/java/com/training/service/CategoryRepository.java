package com.training.service;

import org.springframework.data.repository.CrudRepository;

import com.training.model.Category;

public interface CategoryRepository extends CrudRepository<Category,Long> {

}
