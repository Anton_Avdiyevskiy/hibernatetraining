//package com.training.service;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.stereotype.Service;
//
//import com.training.model.Account;
//
//@Service
//public class UserService {
//
//	@Autowired
//	private UserRepository userRepo;
//	
//	@Autowired
//	private BCryptPasswordEncoder bCryptPasswordEncoder;
//	
//	public Account findUser(String userName) {
//		Account account = userRepo.findByUsername(userName);
//		return account;
//	}
//	
//	public void saveUser(Account account) {
//		String password = account.getPassword();
//		String encodedPassword = bCryptPasswordEncoder.encode(password);
//		account.setPassword(encodedPassword);
//		userRepo.save(account);
//	}
//}
