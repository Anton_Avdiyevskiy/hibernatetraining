package com.training.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.training.exception.NotFoundEntityException;
import com.training.model.Brand;
import com.training.model.Category;

import static com.training.exception.APIMessage.*;

@Service
public class BrandService {

	@Autowired
	private BrandRepository brandRepository;

	@Autowired
	private CategoryRepository categoryRepository;

	public Brand getBrandById(long brandId) {
		return brandRepository.findById(brandId).orElseThrow(() -> new NotFoundEntityException(BRAND_NOT_FOUND));
	}

	public Brand saveBrand(Brand brand) {

		brandRepository.save(brand);
		return brand;
	}

	public void deleteBrand(long brandId) {
		Brand brand = brandRepository.findById(brandId)
				.orElseThrow(() -> new NotFoundEntityException("brand with such id is absent"));
		brandRepository.delete(brand);
	}

	public Brand updateBrand(long id, Brand newBrand) {
		brandRepository.findById(id).map(brand -> {
			brand.setName(newBrand.getName());
			brand.setRating(newBrand.getRating());
			return brandRepository.save(brand);
		}).orElseThrow(() -> new NotFoundEntityException("brand with such id is absent"));
		return newBrand;
	}

	public Brand addCategoryToBrand(long brandId, long categoryId) {
		Brand brand = brandRepository.findById(brandId).orElseThrow(() -> new NotFoundEntityException(BRAND_NOT_FOUND));
		Category category = categoryRepository.findById(categoryId)
				.orElseThrow(() -> new NotFoundEntityException(CATEGORY_NOT_FOUND));
		brand.getCategories().add(category);
		category.getBrands().add(brand);
		brandRepository.save(brand);
		return brand;
	}

	public List<Category> getCategoriesOfBrand(long brandId) {
		Brand brand = brandRepository.findById(brandId).orElseThrow(() -> new NotFoundEntityException(BRAND_NOT_FOUND));
		List<Category> categories = brand.getCategories();
		if (categories.isEmpty()) {
			throw new NotFoundEntityException(CATEGORY_NOT_FOUND);
		}
		return categories;

	}

}
