package com.training.service;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.training.exception.NotFoundEntityException;
import com.training.model.Category;

@Service
public class CategoryService {

	@Autowired
	private CategoryRepository categoryRepository;

	public void saveCategory(Category category) {
		categoryRepository.save(category);
	}

	public Category getCategory(Long id) {
		Category category = categoryRepository.findById(id).orElseThrow(()->new NotFoundEntityException("NO_CATEGORY_FOUND"));
		return category;
	}

	public void deleteCategory(Long id) {
		Category category = categoryRepository.findById(id).orElseThrow(()->new NotFoundEntityException("Category with such id is absent"));
		categoryRepository.delete(category);
	}
	
	public void updateCategory(Long id,Category newCategory) {
		categoryRepository.findById(id).map(category->{
			category.setName(newCategory.getName());
			category.setAmountOfGoods(newCategory.getAmountOfGoods());
			return categoryRepository.save(category);
		}).orElseThrow(()->
			new NotFoundEntityException("Category with such id is absent")
		);
	}
	
	public List<Category> getAllCategories(){
		return (List<Category>) categoryRepository.findAll();
	}
}
