package com.training.service;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.training.model.Product;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {

}
