package com.training.service;

import org.springframework.data.repository.CrudRepository;

import com.training.model.Account;


public interface UserRepository extends CrudRepository<Account,Long> {

	public Account findByUsername(String username);
}
