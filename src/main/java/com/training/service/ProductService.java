package com.training.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.training.exception.NotFoundEntityException;
import com.training.model.Category;
import com.training.model.Product;

import static com.training.exception.APIMessage.*;

@Service
public class ProductService {

	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private CategoryRepository categoryRepository;

	public void save(Product product,long categoryId) {
		Category category = categoryRepository.findById(categoryId).orElseThrow(() -> new NotFoundEntityException(CATEGORY_NOT_FOUND));
		category.getProducts().add(product);
		product.setCategory(category);
		productRepository.save(product);
	}

	public Product getProduct(long id) {
		Product product = productRepository.findById(id)
				.orElseThrow(() -> new NotFoundEntityException(PRODUCT_NOT_FOUND));
		return product;
	}

	public void deleteProduct(long productId,long categoryId) {
		Product product = productRepository.findById(productId)
				.orElseThrow(() -> new NotFoundEntityException(PRODUCT_NOT_FOUND));
		Category category = categoryRepository.findById(categoryId).orElseThrow(() -> new NotFoundEntityException(CATEGORY_NOT_FOUND));
		category.getProducts().remove(product);
		productRepository.delete(product);
	}
	
	public void updateProduct(Product newProduct,long productId) {
		 productRepository.findById(productId)
		 .map(p->{
			 p.setAmount(newProduct.getAmount());
			 p.setDescription(newProduct.getDescription());
			 p.setModel(newProduct.getModel());
			 p.setName(newProduct.getName());
			 p.setYearOfProduction(newProduct.getYearOfProduction());
			 return productRepository.save(p);
		 }).orElseThrow(()-> new NotFoundEntityException(PRODUCT_NOT_FOUND));
	}
}
