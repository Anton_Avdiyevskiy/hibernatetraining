package com.training.app;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import com.training.exception.NotFoundEntityException;
import com.training.model.Brand;
import com.training.model.Category;
import com.training.service.BrandRepository;
import com.training.service.BrandService;
import com.training.service.CategoryRepository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.*;
import static com.training.exception.APIMessage.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BrandServiceTest {

	private static final long TEST_BRAND_ID = 23345;

	@Mock
	private BrandRepository brandRepository;

	@Mock
	private CategoryRepository categoryRepository;

	@Mock
	private Brand brand;

	@Mock
	private List<Category> categories;

	@InjectMocks
	private BrandService brandService;
	

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getBrandByIdTest_successfull() {

		when(brandRepository.findById(TEST_BRAND_ID)).thenReturn(Optional.of(new Brand("LG")));
		Brand brand = brandService.getBrandById(TEST_BRAND_ID);
		assertNotNull(brand);

	}

	@Test(expected = NotFoundEntityException.class)
	public void getBrandByIdTest_failed() {
		when(brandRepository.findById(TEST_BRAND_ID)).thenThrow(new NotFoundEntityException(BRAND_NOT_FOUND));
		Brand brand = brandService.getBrandById(TEST_BRAND_ID);
		assertNull(brand);
	}

	@Test
	public void getCategoriesOfBrand_successfull() {
		List<Category> categories = Arrays.asList(new Category("Multimedia"));
		when(brandRepository.findById(TEST_BRAND_ID)).thenReturn(Optional.of(brand));
		when(brand.getCategories()).thenReturn(categories);
		List<Category> returnedCategories = brandService.getCategoriesOfBrand(TEST_BRAND_ID);
		assertNotNull(returnedCategories);
	}

	@Test
	public void getCategoriesOfBrand_failed() {

		when(brandRepository.findById(TEST_BRAND_ID)).thenReturn(Optional.of(brand));
		when(brand.getCategories()).thenReturn(categories);
		when(categories.isEmpty()).thenReturn(true);
		try {
			List<Category> returnedCategories = brandService.getCategoriesOfBrand(TEST_BRAND_ID);
		} catch (NotFoundEntityException e) {
			System.out.println(e.getMessage());
			assertEquals(e.getMessage(), CATEGORY_NOT_FOUND);
		}
	}
	
}
